#!/bin/bash

source /opt/ros/indigo/setup.bash
source ~/catkin_ws/devel/setup.bash
export ROS_MASTER_URI=http://mezcity:11311
export ROS_HOSTNAME=192.168.1.12

exec "$@"

