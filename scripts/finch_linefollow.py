#!/usr/bin/env python
import rospy
import time
import colorsys
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd
from finch_bot.msg import finch_feedback
from finch_bot.msg import finch_color
from finch_bot.msg import finch_follow_cmd

# Tune these
GAIN = -1000.0;
MAXSPEED = 0.70; #Split between wheels;

#These are constants
MODE_STOP = finch_follow_cmd.MODE_STOP;
MODE_LINE = finch_follow_cmd.MODE_LINE;
MODE_DEAD = finch_follow_cmd.MODE_DEAD;

RED   = 0;
GREEN = 1;
BLUE  = 2;
CLEAR = 3;

COLOR_NAMES = ["RED", "GREEN", "BLUE", "CLEAR"];

class finch_linefollow(object):

    def process(self):
        mc = finch_motor_cmd();
        ac = finch_aux_cmd();
        now = time.time();

        mc.left = 0;
        mc.right = 0;
	lr = 0;

	global GAIN

        lhsv = colorsys.rgb_to_hsv(self.left_colors[0]/255, self.left_colors[1]/255, self.left_colors[2]/255);
        rhsv = colorsys.rgb_to_hsv(self.right_colors[0]/255, self.right_colors[1]/255, self.right_colors[2]/255);
        if(self.mode == MODE_LINE):

            lr = GAIN*(lhsv[2] / (rhsv[2] + lhsv[2]) - 0.5);
            mc.left  = min(1,max(0,MAXSPEED * (0.50 + lr)));
            mc.right = min(1,max(0,MAXSPEED * (0.50 - lr)));

        elif(self.mode == MODE_DEAD):
            now = time.time();
            if(now - self.turn_start > self.turn_time):
                self.mode = MODE_LINE;
                self.process();
                return
            mc.left  = self.left_mtr;
            mc.right = self.right_mtr;

        elif(self.mode == MODE_STOP):
            mc.left  = 0;
            mc.right = 0;

        self.counter = self.counter + 1;
        if self.counter % 10 == 0 :
            rospy.loginfo("Left %3.2f Right %3.2f",mc.left, mc.right);
            rospy.loginfo("Blend %f GAIN %f", lr, GAIN);
            if(self.mode == MODE_STOP):
                rospy.logerr("Stopped");
            elif(self.mode == MODE_LINE):
                pass
            elif(self.mode == MODE_DEAD):
                rospy.logerr("Reckon %f seconds", now - self.turn_start > self.turn_time);

        ac.led = [ 255 * mc.left, 0, 255*mc.left];
        self.pub.publish(mc)
        self.pub_aux.publish(ac)
	GAIN = rospy.get_param('line_gain');

    def color_cb(self, msg):
        if(msg.id == 1):
            self.left_colors[RED]   = float(msg.rgbc[RED])
            self.left_colors[GREEN] = float(msg.rgbc[GREEN])
            self.left_colors[BLUE]  = float(msg.rgbc[BLUE])
            self.left_colors[CLEAR]  = float(msg.rgbc[CLEAR])
        else:
            self.right_colors[RED]   = float(msg.rgbc[RED])
            self.right_colors[GREEN] = float(msg.rgbc[GREEN])
            self.right_colors[BLUE]  = float(msg.rgbc[BLUE])
            self.right_colors[CLEAR]  = float(msg.rgbc[CLEAR])
            return

        self.process();

    def follow_cb(self, msg):
        self.mode = msg.mode;
        if(msg.mode == msg.MODE_STOP):
            pass
        elif(msg.mode == msg.MODE_LINE):
            #self.color_mode = msg.color_mode;
            self.color_mode = CLEAR;
        elif(msg.mode == msg.MODE_DEAD):
            self.turn_start = time.time();
            self.turn_time = msg.time;
            self.left_mtr = msg.left_mtr;
            self.right_mtr = msg.right_mtr;
        else:
            printf("Unknown msg mode? %s", msg.mode);

        self.process();


    def __init__(self, *args, **kwds):

        self.mode = MODE_LINE;
        self.color_mode = RED;

        self.turn_start = time.time();
        self.turn_time = 0;
        self.left_mtr = 0;
        self.right_mtr = 0;

        self.left_colors = [0, 0, 0, 0];
        self.right_colors = [0, 0, 0, 0];
        self.counter = 0

        rospy.init_node('finch_linefollow', anonymous=True)
	rospy.logfatal('HELLO');

	while not rospy.has_param('line_gain'):
		rospy.logerr('NO GAIN');
		rospy.sleep(1);
		
	global GAIN
	GAIN = rospy.get_param('line_gain');
	rospy.logfatal('GAIN %f', GAIN);

        self.pub = rospy.Publisher('finch_motor_cmd', finch_motor_cmd, queue_size=10)
        self.pub_aux = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)

        rospy.Subscriber('finch_color', finch_color, self.color_cb)
        rospy.Subscriber('finch_follow', finch_follow_cmd, self.follow_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_linefollow();
