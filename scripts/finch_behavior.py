#!/usr/bin/env python
import rospy
import time
import sys
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_follow_cmd
from finch_bot.msg import finch_joy_cmd
from visualization_msgs.msg import Marker

# tune these
SPEED = 0.60;
TIME = 0.7;

RED = 0;
GREEN = 1;
BLUE = 2;

#These are constants
MODE_STOP = finch_follow_cmd.MODE_STOP;
MODE_LINE = finch_follow_cmd.MODE_LINE;
MODE_DEAD = finch_follow_cmd.MODE_DEAD;

def get_traffic_light_state(wpt, lights):
    tl_id = wpt['traffic_light_id']

    if tl_id == -1:
        print 'Trying to find the traffic light state for waypoint with no light!'

    # Because lights are in order
    pair_idx = int((tl_id-1) / 2)
    if lights[pair_idx]['light1'][0]['id'] == tl_id:
        return lights[pair_idx]['light1'][0]['color']

    if lights[pair_idx]['light2'][0]['id'] == tl_id:
        return lights[pair_idx]['light2'][0]['color']

    print 'Could not find traffic light ID ' + tl_id + ' and waypoint ID ' + wpt.id
    sys.exit()


class finch_behavior(object):

    def localization_marker(self):
        m = Marker();
        m.header.frame_id = "/world";
        m.header.stamp = rospy.Time();
        m.id = self.robot_id
        m.type = Marker.SPHERE;
        m.action = Marker.DELETE;

        if(self.current_wpt != -1):
            m.pose.position.x = self.current_wpt['x'];
            m.pose.position.y = self.current_wpt['y'];

        m.pose.position.z = 1;
        m.pose.orientation.x = 0.0;
        m.pose.orientation.y = 0.0;
        m.pose.orientation.z = 0.0;
        m.pose.orientation.w = 1.0;

        m.scale.x = 1.0;
        m.scale.y = 1.0;
        m.scale.z = 0.1;

        m.color.a = 1.0;
        if(self.mode == MODE_STOP):
            m.color.r = 1.0;
            m.color.g = 0.0;
            m.color.b = 0.0;
        elif(self.mode == MODE_LINE):
            m.color.r = 0.0;
            m.color.g = 1.0;
            m.color.b = 0.0;
        elif(self.mode == MODE_DEAD):
            m.color.r = 0.5;
            m.color.g = 0.5;
            m.color.b = 0.5;

        return m;


    def publish(self):
        fc = finch_follow_cmd();
        fc.mode = self.mode;
        fc.color_mode = self.color_mode;
        if(self.mode == MODE_DEAD):
            self.mode = MODE_LINE;
            fc.time = TIME;
            fc.left_mtr = SPEED;
            fc.right_mtr = SPEED;
            if(self.turn_mode == finch_joy_cmd.LEFT_TURN):
                fc.left_mtr = 0;
            if(self.turn_mode == finch_joy_cmd.RIGHT_TURN):
                fc.right_mtr = 0;

        if(self.paused == True):
            fc.mode = MODE_STOP;

        rospy.logerr("pubbing");
        self.pub.publish(fc);

        # Draw localization solution (circle at the current node)
        # Circle is green for traveling, red for waiting, grey for stopped
        pose_marker = self.localization_marker();
        self.marker_pub.publish(pose_marker)

    def joy_cb(self, msg):
        if(msg.robot == msg.ALL_ROBOTS or msg.robot == self.robot_id):
            if(msg.mode == msg.MODE_STOP):
                self.paused = True;
                self.publish();
            elif(msg.mode == msg.MODE_LINE):
                self.paused = False;
                self.mode = MODE_LINE;
                self.publish();
            elif(msg.mode == msg.MODE_DEAD):
                self.paused = False;
                self.mode = MODE_DEAD;
                self.turn_mode = msg.turn_mode;
                self.publish();

    def mfrc_cb(self, msg):

        # If we're at a new tag
        rospy.logerr("arrive beacon %d", msg.uid);
        if(self.rfid != msg.uid and self.rfid != self.old_rfid):
            self.old_rfid = self.rfid;
            self.rfid = msg.uid;

            #self.waypoints = rospy.get_param('/waypoints');
            for wpt in self.waypoints:
                if wpt['id'] != msg.uid: #I'm not localized
                    continue;

                # Record current waypoint for visualization purposes
                self.current_wpt = wpt;

                if wpt['traffic_light_id'] < 0: # Theres no light here
                    continue;

                light_color = "";
                lights = rospy.get_param('/traffic_light_pairs');
                light_color = get_traffic_light_state(wpt, lights)

                rospy.logerr("Light at wpt %d: %s", msg.uid, light_color);
                if (light_color == "green"):
                    break

                self.mode = MODE_STOP;
                self.publish();
                return;

        self.mode = MODE_LINE;
        self.publish();

    def __init__(self, *args):

        rospy.init_node('finch_behavior', anonymous=True)

        self.paused = False;

        self.rfid = -3;
        self.old_rfid = -4;

        self.mode = MODE_LINE;
        self.color_mode = RED;
        self.turn_mode = finch_joy_cmd.NO_TURN;
        self.current_wpt = -1

        # DEBUGGING: print out all parameters to test connection
    	foo = rospy.get_param_names();
    	for s in foo:
    		print s;
    		rospy.logfatal(s);

    	while not rospy.is_shutdown():
    		try:
    			self.waypoints = rospy.get_param('/waypoints');
    		except KeyError:
    			print "KEY MISS";
    			rospy.logfatal('KEY MISS');
    			rospy.sleep(1.);
    		else:
    			break;

    	if len(sys.argv) < 2:
    	    rospy.logfatal('Finch Behavior must have an argument that is the robot ID number!');
    	    sys.exit()

        self.robot_id = int(sys.argv[1])
        rospy.logwarn('Starting finch_behavior for finch %d', self.robot_id);

        self.pub = rospy.Publisher('finch_follow', finch_follow_cmd, queue_size=10)
        self.marker_pub = rospy.Publisher('finch_marker', Marker, queue_size=10)
        rospy.Subscriber('finch_mfrc', finch_mfrc522, self.mfrc_cb)
        rospy.Subscriber('/finch_joy_cmd', finch_joy_cmd, self.joy_cb)

        #rospy.sleep(5);

        #self.publish();
        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = finch_behavior(sys.argv);
