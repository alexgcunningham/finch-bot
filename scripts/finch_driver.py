#!/usr/bin/env python
import rospy
import thread
from finch import Finch
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd
from finch_bot.msg import finch_feedback

class finch_driver(object):

    def motor_cb(self, cmd):
        #rospy.loginfo(rospy.get_caller_id() + 'I heard:')
        if cmd.halt:
            self.finch.halt()
            return

        self.finch.wheels(cmd.left, cmd.right)

    def aux_cb(self, aux):
        #rospy.loginfo(aux)

        self.finch.led(int(aux.led[0]), int(aux.led[1]), int(aux.led[2]));
        if aux.buzz_time :
            if aux.buzz_delay:
                self.finch.buzzer_with_delay(aux.buzz_time, aux.buzz_tone);
            else:
                self.finch.buzzer(aux.buzz_time, aux.buzz_tone);

    def feedback(self, t, s):

        rate = rospy.Rate(1) # 10hz
        while not rospy.is_shutdown():

            temp = self.finch.temperature();
            x, y, z, tap, shake = self.finch.acceleration()

            fb = finch_feedback()
            fb.temp_c = 0; #temp;
            fb.accel[0] = x
            fb.accel[1] = y
            fb.accel[2] = z
            fb.tap      = tap
            fb.shake    = shake
            fb.left_obs, fb.right_obs = self.finch.obstacle();
            fb.left_light, fb.right_light = self.finch.light()

            #rospy.loginfo(fb)
            self.pub.publish(fb)

            rate.sleep()



    def __init__(self, *args, **kwds):

        self.finch = Finch();

        rospy.init_node('finch_drive', anonymous=True)

        self.pub = rospy.Publisher('finch_feedback', finch_feedback, queue_size=10)

        rospy.Subscriber('finch_motor_cmd', finch_motor_cmd, self.motor_cb)
        rospy.Subscriber('finch_aux_cmd', finch_aux_cmd, self.aux_cb)

        thread.start_new_thread(self.feedback, ("thread", self) )

        rospy.spin()

    def __del__(self):
        self.finch.wheels(0, 0)
        self.finch.led(0,0,0);
        self.finch.close();

if __name__ == '__main__':
    fd = finch_driver();
